const express = require('express');
const router = express.Router();
const pool = require('../database');
const  movements = require('../controllers/movements');

router.put('/recharge',movements.recharge);
router.put('/to_download',movements.to_download);
router.put('/user_transfer',movements.user_transfer);
router.put('/payments',movements.payments);
//router.get('/history',movements.history);



module.exports=router;


