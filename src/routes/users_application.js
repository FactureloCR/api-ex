const express = require('express');
const router = express.Router();
const pool = require('../database');
const  users_application = require('../controllers/users_application');


router.post('/insert_user',users_application.insert_user);
router.post('/login_user',users_application.login_user);
router.put('/update_user',users_application.updateUser);
router.put('/change_token',users_application.ChangeToken);
router.delete('/delete_user',users_application.deleteUser);


module.exports=router;