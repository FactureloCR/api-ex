const express = require('express');
const router = express.Router();
const pool = require('../database');
const  users = require('../controllers/users');

router.post('/block_user', users.block_user);



module.exports = router;