const express = require('express');
const router = express.Router();
const pool = require('../database');
const  search = require('../controllers/search');


router.post('/users',search.users);
router.post('/users_code',search.users_code);
router.post('/keyword',search.keyword);
router.post('/date',search.date);
router.post('/date_general',search.date_general);
router.post('/code',search.code);






module.exports=router;