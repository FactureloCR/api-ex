const express = require('express');
const router = express.Router();
const pool = require('../database');
const  e_x = require('../controllers/e_x');


router.post('/insert',e_x.insert);
router.put('/blocked',e_x.blocked);
router.put('/unlock',e_x.unlock);
router.post('/getdata',e_x.getdata);
router.post('/reorganize',e_x.reorganize);
router.post('/create',e_x.create);
router.post('/edit',e_x.edit);
router.delete('/delete',e_x.delete);
router.post('/exist_token',e_x.exist_token);





module.exports=router;
