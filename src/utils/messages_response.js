// CONSTANTS OF INFORMATION
module.exports ={
	 status_false:403,
	 status_true:200,
     status_incomplete:403,
     status_incorrect:500,
     description_correct:"sucess",
     description_incorrect:"error",
     description_incomplete:"data is incomplete,check parameters",
     type_primary:'premium',
     type_second:'platinum',
     type_thirdy:'basic',
     money_primary:'CRC',
     money_secondary:'USD',
     recharge:'recharge',
     to_download:'to_download',
     user_transfer:'user_transfer'
}