const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const session = require('express-session');
const validator = require('express-validator');
const MYSQLStore = require('express-mysql-session')(session);
const { database } = require('./keys');
const bodyParser = require('body-parser');
const path = require('path');
const multer = require('multer');
const app=express();




//Middlewares
app.use(morgan('dev'));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use (function (error, req, res, next){
    //Catch json error
    	res.status(500);
  		res.json({status:500,text:'incorrect data: see documentation'});
});


app.use(session({
	secret :'usermysqlnodeex',
	resave :false,
	saveUinitialized : false,
	store: new MYSQLStore(database)
}));



//Routes

app.set('views', path.join(__dirname, 'views'));
 app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
  }));
  app.set('view engine', '.hbs');

   
    app.use(multer({dest: path.join(__dirname, '../public/upload/temp')}).single('image'));

  app.use('/public', express.static(path.join(__dirname, '../public')));



app.use(require('./routes/index'));
app.use('/users_application',require('./routes/users_application'));
app.use('/users',require('./routes/users'));
app.use('/e_x',require('./routes/e_x'));
app.use('/search',require('./routes/search'));

app.use('/movements',require('./routes/movements'));
app.use('/sockets',require('./routes/sockets'));
 app.use('/public', express.static(path.join(__dirname, './public')));


//app.use('/owners',require('./routes/owners'));








module.exports=app;
