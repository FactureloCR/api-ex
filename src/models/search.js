const mld ={}
const pool = require('../database');

mld.get_data_by_idUser=async(id_wpuser)=>{
	const rows= await pool.query('SELECT *,code FROM `history_ex`,ex_bracelets WHERE history_ex.id_e_x=ex_bracelets.id and id_wpuser=?',[id_wpuser]);
	if(rows.length>0){
		return rows;
	}

	return false;
}

mld.search_keyword=async(keyword)=>{
	const rows= await pool.query(`SELECT * FROM ex_bracelets,wordpress_user WHERE (ex_bracelets.id LIKE '%${keyword}%'
																				 OR wordpress_user.name LIKE '%${keyword}%' 
																				 OR wordpress_user.last_name LIKE '%${keyword}%'
																				  OR wordpress_user.identification_card LIKE '${keyword}%' 
																				  OR wordpress_user.email LIKE '%${keyword}%' 
																				  OR ex_bracelets.code LIKE '%${keyword}%' 
																				  OR ex_bracelets.code LIKE '%${keyword}%' 
																				  OR wordpress_user.id_user LIKE '%${keyword}%')
																				 AND wordpress_user.id_user=ex_bracelets.id_wpuser`);
	if(rows.length>0){
		return rows;
	}
	return 0;
}

mld.search_date_by_idUser=async(idUser,date_initial,date_final)=>{
	const rows= await pool.query(`SELECT id_e_x,type_movement,code,history_ex.amount_initial,amount_recharge,amount_final,description,history_ex.origin,history_ex.date
	 FROM history_ex,ex_bracelets,wordpress_user 
	 WHERE history_ex.id_e_x=ex_bracelets.id and 
	  ex_bracelets.id_wpuser=wordpress_user.id_user 
	  AND history_ex.date>'${date_initial}' and history_ex.date<'${date_final}' and wordpress_user.id_user=${idUser}`);
	if(rows.length>0){
		return rows;
	}
	return 0;
}

mld.search_date_general=async(date_initial,date_final)=>{
	const rows= await pool.query(`SELECT id_e_x,type_movement,code,history_ex.amount_initial,amount_recharge,amount_final,description,history_ex.origin,history_ex.date
	 FROM history_ex,ex_bracelets,wordpress_user 
	 WHERE history_ex.id_e_x=ex_bracelets.id and 
	  ex_bracelets.id_wpuser=wordpress_user.id_user 
	  AND history_ex.date>'${date_initial}' and history_ex.date<'${date_final}'`);
	if(rows.length>0){
		return rows;
	}
	return 0;

}


module.exports=mld;