const mld ={}
const pool = require('../database');



mld.block_user=async(id)=>{
	const rows= await pool.query('UPDATE wordpress_user SET activate=0 WHERE id_user=? ',[id]);
}
mld.create_user=async(new_user)=>{
	const rows=await pool.query('INSERT INTO wordpress_user SET ?',[new_user]);
}
mld.exist_user_by_identification=async(identification_card)=>{
	const rows=await pool.query('SELECT * FROM wordpress_user WHERE identification_card=?',[identification_card]);
	if(rows.length>0){
		return true;
	}

	return false;
}
mld.exist_id_user=async(id_user)=>{
	const rows=await pool.query('SELECT * FROM wordpress_user WHERE id_user=?',[id_user]);
	if(rows.length>0){

		return true;
	}
	return false;
}
mld.get_data_by_idUser=async(id_user)=>{
	const rows= await pool.query("SELECT * FROM wordpress_user WHERE id_user=?",[id_user]);
	if(rows.length>0){

		return rows[0];
	}
	return false;
}

module.exports=mld;