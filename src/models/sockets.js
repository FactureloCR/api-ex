const mdl={};
const pool = require('../database');
mdl.insert=async(id_wpuser,id_socket)=>{
	date =  new Date();
	const newSocket={
		id_wpuser,
		id_socket,
		date
	}
	const rows= await pool.query('INSERT INTO temp_sockets SET ?',[newSocket]);
};

mdl.getidUser_by_code_id=async(id)=>{
	const rows = await pool.query('SELECT * FROM ex_bracelets WHERE id=?',[id]);
	if(rows.length>0){

	return rows[0];

	}

	return false;
}

mdl.get_all_codes_by_id=async(id_wpuser)=>{
	const rows = await pool.query('SELECT * FROM ex_bracelets WHERE id_wpuser=?',[id_wpuser]);
	if(rows.length>0){

		return rows;
	}

	return false;
}

mdl.get_all_id_sockets=async(id_wpuser)=>{
	const rows = await pool.query('SELECT * FROM temp_sockets WHERE id_wpuser=?',[id_wpuser]);
	if(rows.length>0){
		return rows;
	}
	return false;
}
mdl.delete_socket=async(id_socket)=>{
	
	const rows = await pool.query('DELETE FROM temp_sockets WHERE id_socket=?',[id_socket]);
}


module.exports=mdl;