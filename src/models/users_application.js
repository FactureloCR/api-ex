const mld ={}
const pool = require('../database');

mld.login_superadmin=async(username,password)=>{
	const rows=await pool.query('SELECT * FROM application_user WHERE id=0 AND username=? AND password=?',[username,password]);
	 if (rows.length > 0) {
	 		return true;
	 }

	 return false;
}

mld.exist_user=async(username)=>{
	const rows=await pool.query('SELECT * FROM application_user WHERE username=?',[username]);
	if(rows.length >0){
		return true;
	}
	return false;
}

mld.create_user=async(username,password,key_private)=>{
	const newUser={
		username,
		password,
		key_private
	}
	const rows=await pool.query('INSERT INTO application_user SET ?',[newUser]);
}

mld.verificate_user=async(username,password)=>{
	const rows=await pool.query('SELECT * FROM application_user WHERE username=? AND password=?',[username,password]);
	if(rows.length>0){
		return true;
	}

	return false;
}

mld.login_user=async(username,password)=>{
	const rows= await pool.query('SELECT * FROM application_user WHERE username=? AND password=?',[username,password]);
	if(rows.length>0){
		const user= rows[0];
		return user;
	}
	return false;
}

mld.updateUser=async(username,password,key_private)=>{

	const updateUser={
		username,
		password
	}
	const rows= await pool.query('UPDATE application_user SET ? WHERE key_private=?',[updateUser,key_private]);
}

mld.verificate_user_by_token=async(key_private)=>{
	const rows = await pool.query('SELECT * FROM application_user WHERE key_private=?',[key_private]);
	if(rows.length>0){
		return true;
	}

	return false;
}

mld.ChangeToken=async(username,password,key_private)=>{
	const rows = await pool.query('UPDATE application_user user SET key_private=? WHERE username=? AND password=?',[key_private,username,password]);
}

mld.get_datauser_by_token=async(key_private)=>{
	const rows=await pool.query('SELECT * FROM application_user WHERE key_private=?',[key_private]);
	if(rows.length>0){
		return rows[0];
	}

	return false;
}

mld.blockq_user=async(id)=>{
	const rows=await pool.query('SELECT * FROM application_user WHERE activate=0 AND id=?',[id]);
	if(rows.length>0){
		return true;
	}
	return false;
}

mld.deleteUser=async(username,password)=>{
	const rows= await pool.query('UPDATE application_user SET activate=0 WHERE username=? AND password=?',[username,password]);
}



module.exports=mld;