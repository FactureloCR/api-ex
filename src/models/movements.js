const mdl={};
const pool = require('../database');

mdl.get_current_amount=async(code)=>{

const rows = await pool.query('SELECT * FROM ex_bracelets WHERE code=?',[code]);
if(rows.length>0){
	const amount = rows[0].current_amount;
	return amount;
}


return 0;



}

mdl.amount_update=async(code,current_amount)=>{
	const rows= await pool.query('UPDATE ex_bracelets SET current_amount=? WHERE code=?',[current_amount,code]);
}
mdl.history_update=async(id_e_x,date,type_movement,amount_initial,amount_recharge,amount_final,description,origin,transaction_token,currency,id_event)=>{
	const new_history={
		id_e_x,
		date,
		type_movement,
		amount_initial,
		amount_recharge,
		amount_final,
		description,
		origin,
		transaction_token,
		currency,
		id_event
	}

	const rows=await pool.query('INSERT INTO history_ex SET ?',[new_history]);
}
mdl.exist_transaction_by_token=async(transaction_token)=>{
	const rows= await pool.query('SELECT * FROM history_ex WHERE transaction_token=?',[transaction_token]);
	if(rows.length>0){
		return true;
	}
	return false;
}

mdl.get_data_by_token_transaction=async(transaction_token)=>{
	const rows= await pool.query('SELECT * FROM history_ex WHERE transaction_token=?',[transaction_token]);
	if(rows.length>0){
		const users=rows[0];
		return users;
	}
	return 0;
}

mdl.updateActivate=async(activate,api_response,transaction_token)=>{
	const new_activate={
		activate,
		api_response
	}
 const rows=await pool.query('UPDATE history_ex SET ? WHERE transaction_token=?',[new_activate,transaction_token]);
}

mdl.is_activate=async(id_e_x)=>{
	const rows=await pool.query('SELECT * FROM history_ex WHERE id_e_x=?  ORDER BY id DESC LIMIT 1',[id_e_x]);
	if(rows.length>0){
		const users=rows[0];
		if(users.api_response!=""){
			return true;
		}
	}
	return false;
}
mdl.is_recharge=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex WHERE  amount_initial<>"0" AND code=?',code);
	if(rows.length>0){
		 return  true;
	}
	return false;
}




module.exports=mdl;