const mld={}
const pool = require('../database');
mld.verificate_code=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex_bracelets WHERE code=?',[code]);
	if(rows.length>0){
		return true;
	}	
	return false;
	
}
mld.insert=async(amount_initial,id_wpuser,code,creation_date,origin,currency,current_amount,name)=>{
	const new_e_x={
		amount_initial,
		origin,
		code,
		creation_date,
		id_wpuser,
		currency,
		current_amount,
		name


	}

	const rows= await pool.query('INSERT INTO ex_bracelets SET ?',[new_e_x]);
}

mld.upload_e_x=async(imgUrl,id,name)=>{
	
	const rows= await pool.query('UPDATE ex_bracelets SET img=?,name=? WHERE id=?',[imgUrl,name,id]);
}
mld.reorganize=async(code,i)=>{

	const rows= await pool.query("UPDATE ex_bracelets SET position=? WHERE code=?",[i,code]);
}

mld.blocked_e_x=async(code)=>{
	const rows= await pool.query('UPDATE ex_bracelets SET activate=0 WHERE code=? ',[code]);
}
mld.unlock_e_x=async(code)=>{
	const rows = await pool.query('UPDATE ex_bracelets SET activate=1 WHERE code=?',[code]);
}
mld.is_blocked=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex_bracelets WHERE activate=0 AND code=?',[code]);
	if(rows.length>0){
		return true;
	}

	return false;
}
mld.getdata=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex_bracelets WHERE code=?',[code]);
	if(rows.length>0){
		return rows[0];
	}
}
mld.get_data_by_id=async(id)=>{
	const rows = await pool.query('SELECT * FROM ex_bracelets WHERE id=?',[id]);
	if(rows.length>0){
		return rows[0];
	}
}
mld.updatePaid=async(code,current_amount)=>{
	const rows= await pool.query('UPDATE ex_bracelets SET current_amount=? WHERE code=?',[current_amount,code]);
}
mld.get_history=async(code)=>{
	const rows=await pool.query('SELECT * FROM `history_ex`,ex_bracelets where id_e_x=ex_bracelets.id and ex_bracelets.code=?',[code]);
	if(rows.length>0){
		return rows;
	}
	return 0;
}
mld.get_code_by_idUser=async(id_wpuser)=>{
	const rows= await pool.query('SELECT * FROM ex_bracelets WHERE id_wpuser=? and activate=1 ORDER BY position ASC',[id_wpuser]);
	if(rows.length>0){
		return rows;
	}
	return 0;
}
mld.exist_code=async(code)=>{
	const rows=await pool.query('SELECT * FROM ex WHERE code=?',[code]);
	if(rows.length>0){
		return true;
	}else{
		return false;
	}
}

mld.create_code=async(code,amount_initial,type,origin,date_create)=>{
	const new_code={
		code,
		amount_initial,
		type,
		origin,
		date_create
	}
	const rows= await pool.query('INSERT INTO ex SET ?',[new_code]);
}
mld.update_activate=async(code)=>{
	const rows = await pool.query('UPDATE ex SET activate=0 WHERE code=?',[code]);

}
mld.update_ex_bracelets=async(code)=>{

	const rows = await pool.query('UPDATE ex_bracelets SET activate=0 WHERE code=?',[code]);

}

mld.exist_code_insert=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex WHERE activate=1 and code=?',[code]);
	if(rows.length>0){
		return true;
	}
	else{
		return false;
	}

}
mld.get_data_code=async(code)=>{
	const rows= await pool.query('SELECT * FROM ex WHERE code=?',[code]);
	if(rows.length>0){
		return rows[0];
	}
	return false;
}

mld.get_iduser_by_code=async(code)=>{
	const rows= await pool.query("SELECT * FROM ex_bracelets WHERE code=?",[code]);
	if(rows.length>0){
		return rows[0];
	}
	return false;
}




module.exports=mld;