const ctrl={}

// CONSTANTS OF DE MODELS AND DEPENDENCIES
const user_model = require('../models/users');
const user_application = require('../models/users_application');
const search_model = require('../models/search');
const ex_model= require('../models/e_x');

const jwt = require('jsonwebtoken');
const hat = require('hat');

// CONSTANTS OF INFORMATION
const msj=require('../utils/messages_response.js');

ctrl.users=async(req,res)=>{
	const {tokenId,idUser}=req.body;
	if(tokenId && idUser){
		verificate= await user_application.verificate_user_by_token(tokenId);
		if(verificate){
			verificate= await user_model.exist_id_user(idUser);
			if(verificate){
				get_data_by_idUser= await search_model.get_data_by_idUser(idUser);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:get_data_by_idUser});

			}// end if verificate exist user by id

			else{
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'The id user does not exist'});
			}

		}// end  if verificate token
		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
		}


	}// end if isset token idUser

	else{


		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});

	}
}

ctrl.keyword=async(req,res)=>{
	const{tokenId,keyword}=req.body;

	if(tokenId && keyword){
		verificate= await user_application.verificate_user_by_token(tokenId);
		if(verificate){


			search_keyword= await search_model.search_keyword(keyword);
			res.json(search_keyword);

		}// en if verificate token
		else{

			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
		}

	}// end if isset tokenId, iduser
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}

}

ctrl.date=async(req,res)=>{
	const{tokenId,date_initial,date_final,idUser}=req.body;
	
	if(tokenId && date_initial,date_final && idUser){
		verificate= await user_application.verificate_user_by_token(tokenId);
		if(verificate){
			verificate= await user_model.exist_id_user(idUser);
			if(verificate){
				search= await search_model.search_date_by_idUser(idUser,date_initial,date_final);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:search});

			}// end if verificate idUser

			else{
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'The idUser does not exist'});	
			}

		}else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
		}

		//end if verificate token

	}// end if if isset tokenId, date_initial, deag

	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});

	}
}

ctrl.date_general=async(req,res)=>{
	const{tokenId,date_initial,date_final}=req.body;
	if(tokenId && date_initial && date_final){
		verificate= await user_application.verificate_user_by_token(tokenId);
		if(verificate){
			
			search= await search_model.search_date_general(date_initial,date_final);
			res.status(msj.status_true);
			res.json({status:msj.status_true,response:msj.description_correct,text:search});

		}// end if verificate TokenId

		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
		}
	}// end if isset tokenId, date_initial, date_final
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}
ctrl.code=async(req,res)=>{
	const{tokenId,code}= req.body;
	if(tokenId && code){
		let verificate=await user_application.verificate_user_by_token(tokenId);
		if(verificate){
			verificate= await ex_model.verificate_code(code);
			if(verificate){
				data_code= await ex_model.get_history(code);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:data_code});
			}// end if vericate code
			else{
				res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The idUser does not exist'});	
			}

		}// end if verificate TokenId
		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});	
		}
	}
	// end if isset tokenId code
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}
ctrl.users_code=async(req,res)=>{
 const{tokenId,idUser}= req.body;
 	if(tokenId && idUser){
		let verificate=await user_application.verificate_user_by_token(tokenId);
			
			if(verificate){
				verificate= await user_model.exist_id_user(idUser);
				if(verificate){
					data_code= await ex_model.get_code_by_idUser(idUser);
					res.status(msj.status_true);
					res.json({status:msj.status_true,response:msj.description_correct,text:data_code});

				}// end if vericate idUser

				else{
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'The code does not exist'});	
				}

			}// end if vericate code

			else{

				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});	
			}

	}// end if isset tokenId code
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}



module.exports=ctrl;



