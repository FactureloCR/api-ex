const ctrl={}
// CONSTANTS OF DE MODELS AND DEPENDENCIES
const user_model = require('../models/users_application');
const jwt = require('jsonwebtoken');
const hat = require('hat');


// CONSTANTS OF INFORMATION
const msj=require('../utils/messages_response.js');



ctrl.insert_user=async(req,res)=>{
	const {user,password,new_user,new_password} = req.body;

	if(user && password && new_user && new_password){
		let verificate= await user_model.login_superadmin(user,password);
		if(verificate){
			if(new_user.length>0 && new_user.length<50 && new_password.length>0 && new_password.length<50){
				
				verificate= await user_model.exist_user(new_user);
				if(!verificate){
					const hat_code= hat(); 
					const token_user=jwt.sign({new_user:new_user},hat_code);
					const create_user=await user_model.create_user(new_user,new_password,token_user);
					res.status(msj.status_true);
					res.json({status:msj.status_true,response:msj.description_correct,text:token_user});
					
					//create_user = await user_model.create_user(new_user,new_password);

				}//end if verificate exist user
				else{
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'error new_user: username already exist'});
				}	

					
			}
			
			else{

				res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'the length of the data must be less than 0 and greater than 50'});

			} 

		}// end if verificate
		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'permissions denied: the user is not an administrator'});
		}
	
	}// end if isset user && password && new_user && new_password
	else{

		res.status(msj.status_incomplete);
		res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
	}


	
}

ctrl.login_user=async(req,res)=>{
	const {user,password}= req.body;
	if(user && password){

		if(user!=" " && password!=" "){

			verificate=	await user_model.verificate_user(user,password);
			if(verificate){
				data_user=await user_model.login_user(user,password);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{tokenId:data_user.key_private}});

			}// end if verficate user exist
			else{

				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'the user or password does not match'});
			}

		}

		else{

			res.status(msj.status_false);
			res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
		}

	}// end if isset user && password

	else{

		res.status(msj.status_false);
		res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});

	}
}

ctrl.updateUser=async(req,res)=>{
	const {user,password,tokenId}=req.body;
	if(user && password && tokenId ){
		if(user!=" " && password!=" " && tokenId!=" "){
			let verificate=await user_model.verificate_user_by_token(tokenId);
			if(verificate){

				const udpateUser= await user_model.updateUser(user,password,tokenId);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{user:user,password:password}});


			}// end if verificate user,password
			else{
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'the user or password does not match'});
			}

		}
		else{

			res.status(msj.status_false);
			res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
		}

	}
	// end if isset user,password and token
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.ChangeToken=async(req,res)=>{
	const {user,password,tokenId}=req.body;
	if(user && password && tokenId){
		if(user!="" || password!="" || tokenId!=""){
			let verificate=await user_model.verificate_user(user,password);
			if(verificate){
				const hat_code=hat();
				const new_token=jwt.sign({user:user},hat_code);
				const ChangeToken=user_model.ChangeToken(user,password,new_token);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{tokenId:new_token}});
			}
		}// end if isset user,password and token

	}else{
		res.status(msj.status_false);
		res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.deleteUser=async(req,res)=>{
	const{user,password,tokenId}=req.body;
	if(user && password && tokenId){
		if(user!=" " || password!=" " || tokenId!=" "){
			let verificate=await user_model.verificate_user(user,password);
			if(verificate){
				const deleteUser=user_model.deleteUser(user,password);
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:'User Desactive'});

			}// end if verificate user exist 
			else{
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'the user or password does not match'});

			}

		}// end user, password, tokenId is empty
		else{
			res.status(msj.status_false);
			res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
		}
	}else{
		res.status(msj.status_false);
			res.json({status:msj.status_incomplete,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}



module.exports=ctrl;