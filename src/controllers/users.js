const ctrl={}

// CONSTANTS OF DE MODELS AND DEPENDENCIES
const user_model = require('../models/users');
const user_application = require('../models/users_application');
const jwt = require('jsonwebtoken');
const hat = require('hat');

// CONSTANTS OF INFORMATION
const msj=require('../utils/messages_response.js');


ctrl.block_user=async(req,res)=>{
	const {user_admin,password_admin,id_block}=req.body;
	if(user_admin && password_admin && id_block){
		const verificate= await user_application.login_superadmin(user_admin,password_admin);
		if(verificate){
			const  block_user= await user_model.block_user(id_block);
			res.status(msj.status_true);
			res.json({status:msj.status_true,response:msj.description_correct,text:'User block'});

		}else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'permissions denied: the user is not an administrator'});
		}

	}else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}

}






module.exports=ctrl;