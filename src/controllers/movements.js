const ctrl={};
const socket_client = require('socket.io-client')('http://localhost:3003');
const e_x_model= require('../models/e_x');
const wpuser_model = require('../models/users');
const user_model = require('../models/users_application');
const movements = require('../models/movements');
const jwt = require('jsonwebtoken');
const hat = require('hat');
const fetch = require('node-fetch');
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
const bent = require('bent');
let resp="";


// CONSTANTS OF INFORMATION
const msj=require('../utils/messages_response.js');


ctrl.recharge=async(req,res)=>{
	const {code,tokenId,amount,description,currency}=req.body;
	if(code && tokenId && amount && description && currency){
		if(code.length>0 &&  code.length<50 && amount.length>0 && amount.length<50 && description.length>0 && description.length<100){

			let verificate= await e_x_model.is_blocked(code);
			if(verificate){
				res.status(msj.status_true);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'code locked'});
				return true;
			}

			verificate= await e_x_model.verificate_code(code);

		if(verificate){
			verificate= await user_model.verificate_user_by_token(tokenId);
			if(verificate){
				data_user = await user_model.get_datauser_by_token(tokenId);
				const date = new Date();

				current_amount=await movements.get_current_amount(code);
				getdata_code=await e_x_model.getdata(code);
				code_active=await movements.is_activate(getdata_code.id);
				// if(!code_active){
				// 	res.status(msj.status_false);
				// 	res.json({status:msj.status_false,response:msj.description_incorrect,text:'the last payment was not canceled'});
				// 	return true;
				// }

				if(isNaN(Number(amount))){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'this is not a number, valid numbers in 1000.000 format'});
					return true;
				}
				total_amount=Number(current_amount)+Number(amount);

				
				amount_update= await movements.amount_update(code,total_amount);
				const hat_code= hat(); 
				const transaction_token=jwt.sign({code:getdata_code.id,recharge:msj.recharge,date:date,current_amount:current_amount},hat_code);
				history_update= await movements.history_update(getdata_code.id,date,msj.recharge,current_amount,amount,total_amount,description,data_user.id,transaction_token,currency,'0');
				
				code_id=getdata_code.id;
				await socket_client.emit('movements',{code_id});

				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{code:code,
																						current_amount:total_amount,
																						recharge:amount,
																						previous_amount:current_amount,
																						date:date,
																						transaction_token:transaction_token}});

			}
		
		}

		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});

	}// end if  length
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'the length of the data must be less than 0 and greater than 50'});
	}	
	

}// end if isset tokenId and code
else{

		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.to_download=async(req,res)=>{
	const {code,tokenId,amount,description,idEvent,currency}=req.body;
	if(code && tokenId && amount && description && idEvent && currency ){
			let verificate= await e_x_model.is_blocked(code);
			if(verificate){
				res.status(msj.status_true);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'code locked'});
				return true;
			}

		if(isNaN(Number(amount))){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'this is not a number, valid numbers in 1000.000 format'});
					return true;
				}
		 verificate= await e_x_model.verificate_code(code);
		if(verificate){
			verificate= await user_model.verificate_user_by_token(tokenId);
			if(verificate){
				data_user = await user_model.get_datauser_by_token(tokenId);

				current_amount=await movements.get_current_amount(code);
				getdata_code=await e_x_model.getdata(code);
				code_active=await movements.is_activate(getdata_code.id);
				is_recharge=await movements.is_recharge(code);
				if(!code_active){
					if(!is_recharge){

						res.status(msj.status_false);
						res.json({status:msj.status_false,response:msj.description_incorrect,text:'the last payment was not canceled'});
						return true;
					}
					
				}
				total_amount=Number(current_amount)-Number(amount);
				const date = new Date();
				const hat_code= hat(); 
				const transaction_token=jwt.sign({code:getdata_code.id,recharge:msj.recharge,date:date,current_amount:current_amount},hat_code);

				if(total_amount<0){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'the amount exceeds the current balance'});
					
					history_update= await movements.history_update(getdata_code.id,date,msj.to_download,current_amount,amount,total_amount,"transaction error: the amount exceeds the balance",data_user.id,transaction_token,currency,idEvent);
				}else{

					amount_update= await movements.amount_update(code,total_amount);

				history_update= await movements.history_update(getdata_code.id,date,msj.to_download,current_amount,amount,total_amount,description,data_user.id,transaction_token,currency,idEvent);
				get_iduser= await e_x_model.get_iduser_by_code(code);
				data_wpuser=await wpuser_model.get_data_by_idUser(get_iduser.id_wpuser);
				code_id=getdata_code.id;
				await socket_client.emit('movements',{code_id});
				
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{code:code,
																						current_amount:total_amount,
																						to_download:amount,
																						previous_amount:current_amount,
																						date:date,
																						name:data_wpuser.name,
																						last_name:data_wpuser.last_name,
																						identification_card:data_wpuser.identification_card,
																						email:data_wpuser.email
																					}});

				return true;

				}

				



			}
		}
		   res.status(msj.status_false);
		   res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});


	}// end if isset tokenId,code,amount,description

	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}


ctrl.user_transfer=async(req,res)=>{
	const {code,tokenId,code_transfer,amount,description,currency}=req.body;
	if(code && tokenId && code_transfer && amount && description && currency){
		let verificate= await e_x_model.is_blocked(code);
			if(verificate){
				res.status(msj.status_true);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'code locked'});
				return true;
			}

	if(isNaN(Number(amount))){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'this is not a number, valid numbers in 1000.000 format'});
					return true;
				}
	 verificate= await e_x_model.verificate_code(code);
	if(verificate){
		verificate= await user_model.verificate_user_by_token(tokenId);
		if(verificate){
			verificate= await  e_x_model.verificate_code(code_transfer);
			if(verificate){
				data_user = await user_model.get_datauser_by_token(tokenId);
				current_amount=await movements.get_current_amount(code);
				const date = new Date();
				total_amount=Number(current_amount)-Number(amount);
				getdata_code=await e_x_model.getdata(code);
				code_active=await movements.is_activate(getdata_code.id);
				if(!code_active){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'the last payment was not canceled'});
					return true;
				}

				
				const hat_code= hat(); 
				const transaction_token=jwt.sign({code:getdata_code.id,recharge:msj.recharge,date:date,current_amount:current_amount},hat_code);

				if(total_amount<0){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'insufficient funds'});
					history_update= await movements.history_update(getdata_code.id,date,msj.user_transfer,current_amount,amount,total_amount,"insufficient funds",data_user.id,transaction_token,currency,'0');
				}else{


						// reduce money to code
						getdata_code=await e_x_model.getdata(code);

						code_id=getdata_code.id;
						await socket_client.emit('movements',{code_id});

						update_amount= await movements.amount_update(code,total_amount);
						history_update= await movements.history_update(getdata_code.id,date,msj.user_transfer,current_amount,amount,total_amount,description,data_user.id,transaction_token,currency,'0');

						//increase money

						getdata_code=await e_x_model.getdata(code_transfer);

						code_id=getdata_code.id;
						await socket_client.emit('movements',{code_id});

						current_amount=await movements.get_current_amount(code_transfer);
						total_amount=Number(current_amount)+Number(amount);
						update_amount=await movements.amount_update(code_transfer,total_amount);
						history_update= await movements.history_update(getdata_code.id,date,msj.user_transfer,current_amount,amount,total_amount,description,data_user.id,transaction_token,currency,'0');

						res.status(msj.status_true);
						res.json({status:msj.status_true,response:msj.description_correct,text:{origin_code:code,transfer_code:code_transfer,amount:amount,date:date}});

				}

			}
		}
	}

	res.status(msj.status_false);
    res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});



}// end if isset code,tokenId,amount,description
else{
	res.status(msj.status_false);
	res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
}
}

ctrl.payments=async(req,res)=>{
	const{tokenId,transactionToken,number_card,expiration_month,expiration_year,verification_value}=req.body;
	

	if(tokenId && transactionToken && number_card && expiration_month && expiration_year && verification_value){
		verificate=await movements.exist_transaction_by_token(transactionToken);
	if(verificate){
		verificate= await user_model.verificate_user_by_token(tokenId);
		get_data_by_token_transaction= await movements.get_data_by_token_transaction(transactionToken);

		if(verificate){
		 const  data={
			"description":get_data_by_token_transaction.description,
			"currency":get_data_by_token_transaction.currency,
			"aumont":get_data_by_token_transaction.amount_recharge,
			"number_card":number_card,
			"expiration_month":expiration_month,
			"expiration_year":expiration_year,
			"verification_value":verification_value,
			"token":"23a94deb39857e3212493e28609da8b4"
		};
		
	

		fetch('https://payments.facturelocr.info/payments/add', {
        method: 'post',
           mode: 'cors', 
        body:    JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
    })
    .then(res => res.json())
    .then(async (json) =>{
    		if(json.status =='Successful'){
    			status=1;
    			api_response=JSON.stringify(json);
    			await movements.updateActivate(status,api_response,transactionToken);
    			res.status(msj.status_true);
    			res.json({status:msj.status_true,response:msj.description_correct,text:json});

    		}else{
    			api_response=JSON.stringify(json);
    			status=0;
    			await movements.updateActivate(status,api_response,transactionToken);

    			data_code=await e_x_model.get_data_by_id(get_data_by_token_transaction.id_e_x);
    			current_amount=await movements.get_current_amount(data_code.code);
				getdata_code=await e_x_model.getdata(data_code.code);
				code_id=getdata_code.id;
				await socket_client.emit('movements',{code_id});
				total_amount=Number(current_amount)-Number(get_data_by_token_transaction.amount_recharge);

				updatePaid=await e_x_model.updatePaid(data_code.code,total_amount);
    			res.status(msj.status_false);
    			res.json({status:msj.status_false,response:msj.description_incorrect,text:json});
    		}
    });
		

		


	}// end if verificate Token ID
	else{
		res.status(msj.status_false);
    	res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
	}

	}// end if verficate Token Transaction
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'The transaction token not exist'});
	}

}// end if isset tokenId, transactionToken, number_card,expirtation_month....
else{
	res.status(msj.status_false);
    res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
 }
}


module.exports=ctrl;