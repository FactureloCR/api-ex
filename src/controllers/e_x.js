const ctrl={}
const e_x_model= require('../models/e_x');
const wpuser_model = require('../models/users');
const user_model = require('../models/users_application');
const fs = require('fs-extra');
const path = require('path');
const uniqid = require('uniqid');

const jwt = require('jsonwebtoken');
const hat = require('hat');


// CONSTANTS OF INFORMATION
const msj=require('../utils/messages_response.js');


ctrl.insert=async(req,res)=>{
	const{currency,id_user,name,last_name,identification_card,email,code,tokenId,name_code}=req.body;
	amount_initial="0";
	let verificate= await e_x_model.exist_code_insert(code);
	if(!verificate){


		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'This code is not in our database'});
		return true;
	}
	if(id_user && code  && identification_card &&  name_code && email && code && name && last_name && currency && tokenId ){
		
		getdata_ex= await e_x_model.get_data_code(code);
		if(getdata_ex.amount_initial!='0' && getdata_ex.amount_initial!=''){
			amount_initial=getdata_ex.amount_initial;
		}

			if(msj.money_primary!=currency && msj.money_secondary!=currency){
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'the "currency" type must be CRC or USD'});
			}
		 verificate= await wpuser_model.exist_user_by_identification(identification_card);
		if(!verificate){
			verificate=await wpuser_model.exist_id_user(id_user);
			if(verificate){
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'the user_id is already being used by another user'});
			}
			const new_user={
				name,
				last_name,
				identification_card,
				email,
				id_user
			}

			create_user= await wpuser_model.create_user(new_user);
			}


		if(amount_initial.length>0 && amount_initial.length<50 && id_user.length>0 && code.length<50 && 
			identification_card.length>0 && email.length<50 &&
			name.length>0 && name.length<50 && email.length>0 && email.length<50 && currency.length>0 && currency.length<4
			){
				// if(msj.type_primary!=type &&  msj.type_second!=type && msj.type_thirdy!=type){
				// 	res.status(msj.status_false);
				// 	res.json({status:msj.status_false,response:msj.description_incorrect,text:'the "type" field, only allows type entries: premium,platinum,basic'});
				// 	return true;
				// }

		   	  verificate=await e_x_model.verificate_code(code);
		   	  
		   if(!verificate){
		   		verificate= await user_model.verificate_user_by_token(tokenId);

		   		if(verificate){
		   				data_user = await user_model.get_datauser_by_token(tokenId);
		   				blockq_user= await user_model.blockq_user(data_user.id);
		   				if(blockq_user){
		   						res.status(msj.status_false);
		   						res.json({status:msj.status_false,response:msj.description_incorrect,text:'This application has been blocked by the API'});
		   				}
		   				const date= new Date();
		   				e_x_model.insert(amount_initial,id_user,code,date,data_user.id,currency,amount_initial,name_code);
		   				res.status(msj.status_true);
		   				res.json({status:msj.status_true,response:msj.description_correct,text:{amount_initial:amount_initial,
		   																						code:code,
		   																						name_code:name_code,
		   																						create_date:date,
		   																						id_user:id_user,
		   																						origin:data_user.username}});


		   		}// end if exist token

		   }// end if exist code
		   res.status(msj.status_false);
		   res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});

		}// end if length 

		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'the length of the data must be less than 0 and greater than 50'});
		}

	}// end if isset amount_initial, id_user, code
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}



ctrl.blocked=async(req,res)=>{
	const{tokenId,code}=req.body;
	if(tokenId && code){	

		let verificate= await e_x_model.verificate_code(code);
		if(verificate){
			verificate= await user_model.verificate_user_by_token(tokenId);
			if(verificate){
				blocked_e_x=await e_x_model.blocked_e_x(code);
				const date= new Date();
				res.status(msj.status_true);
				res.json({status:msj.status_true,response:msj.description_correct,text:{code_blockquote:code,date:date}});
			}// end if exist token
			else{
				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'The Token does not exist'});
			}

		}// end if exist code

		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The code does not exist'});
		}

	}// end if isset tokenId and code

	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
	
}
ctrl.unlock=async(req,res)=>{
	const{tokenId,code}=req.body;
	if(tokenId && code){
		let verificate= await e_x_model.verificate_code(code);
		if(verificate){
			verificate= await user_model.verificate_user_by_token(tokenId);
				if(verificate){
					unlock_e_x= await e_x_model.unlock_e_x(code);
					const date= new Date();
					res.status(msj.status_true);
					res.json({status:msj.status,response:msj.description_correct,text:{code_unlock:code,date:date}});


				}// end if exist token

				else{

					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'The Token does not exist'});

				}


		}// end if exist code

		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The code does not exist'});
		}

	}// end if isset tokenId and code
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.getdata=async(req,res)=>{
	const{tokenId,code}=req.body;
	if(tokenId && code){
		let verificate= await e_x_model.verificate_code(code);
		if(verificate){
			verificate= await user_model.verificate_user_by_token(tokenId);
			if(verificate){
				getdata=await e_x_model.getdata(code);
				res.json({status:msj.status_true,response:msj.description_correct,text:getdata})
			}
		}

		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});


	}// end if isset tokenId and code
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.create=async(req,res)=>{
	const{tokenId,code,amount_initial,type}=req.body;
	if(tokenId && code && amount_initial && type ){
		let verificate= await user_model.verificate_user_by_token(tokenId);
		if(verificate){
		   data_user = await user_model.get_datauser_by_token(tokenId);		
		   verificate= await e_x_model.exist_code(code);
		if(verificate){
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The code  exist'});
			return true;
		}// end if verificate exist code
		if(isNaN(Number(amount_initial))){
					res.status(msj.status_false);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:'this is not a number, valid numbers in 1000.000 format'});
					return true;
				}// end if is number
		date=new Date();
		insert=await e_x_model.create_code(code,amount_initial,type,data_user.id,date);
		res.status(msj.status_true);
		res.json({status:msj.status_true,response:msj.description_correct,text:'successful process'});

		}// end if verificate token
		else{	

			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist or code of e_x exist'});
		}
	}// end if isset tokenId and code
		else{

			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
		}	
}
ctrl.delete=async(req,res)=>{
	const{tokenId,code}=req.body;
	let verificate= await user_model.verificate_user_by_token(tokenId);
	if(verificate){
		 verificate= await e_x_model.exist_code(code);
		 if(verificate){
		 	update_activate= await e_x_model.update_activate(code);
		 	update_ex_bracelets = await e_x_model.update_ex_bracelets(code);
		 	res.status(200);
		 	res.json({status:msj.status_true,response:msj.description_correct,text:'locked code'});
		 }// end if verificate code
		 else{
		 	res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The code does not exist'});
		 }

	}// end if verificate token
	else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.exist_token=async(req,res)=>{
	const{tokenId}=req.body;
	let verificate= await user_model.verificate_user_by_token(tokenId);
	if(verificate){
		res.status(msj.status_true);
		res.json({status:msj.status_true,response:msj.description_correct,text:'correct'});
	}else{
		res.status(msj.status_false);
		res.json({status:msj.status_false,response:msj.description_incorrect,text:'not exist'});
	}
}

ctrl.block_code_create=async(req,res)=>{
	const{tokenId,code}=req.body;
	if(tokenId && code){
		let verificate= await user_model.verificate_user_by_token(tokenId);
		if(verificate){
				

		}// verificate tokenId
		else{
			res.status(msj.status_false);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
		}

	}// end if isset tokenId, code	
	else{
			res.status(msj.status_true);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	}
}

ctrl.reorganize=async(req,res)=>{
		const{tokenId,order}=req.body;

			if(tokenId  && order){
				let verificate= await user_model.verificate_user_by_token(tokenId);
			if(verificate){

					
					for(i=0;i<order.length;i++){

						await e_x_model.reorganize(order[i]["code"],i);

					}

					res.status(200);
					res.json({status:msj.status_true,response:msj.description_correct,text:'correct'});

			}else{

				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:'The token does not exist'});
			}


			}
			else{

				res.status(msj.status_false);
				res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
			}

}

ctrl.edit=async(req,res)=>{
	  const imageTempPath = req.file.path;
	  if(imageTempPath){
	  		   const imgUrl=uniqid();
	  		   const{id,tokenId,name}=req.body;
	  		  const imageTempPath = req.file.path;
      	 	  const ext = path.extname(req.file.originalname).toLowerCase();
      	     const targetPath = path.resolve(`src/public/upload/${imgUrl}${ext}`);
      		    if (ext === '.png' || ext === '.jpg' || ext === '.jpeg' || ext === '.gif') {

      		    		 await fs.rename(imageTempPath, targetPath);
      		    		 const upload= await e_x_model.upload_e_x(('http://ex.facturelocr.info/'+'public/upload/'+imgUrl+ext),id,name);
      		    		 res.status(msj.status_true);
      		    		res.json({status:msj.status_true,response:msj.description_correct,text:'correct'});
      		    }else{

      		    	res.status(msj.status_true);
					res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
      		    }


	  }else{
	  	res.status(msj.status_true);
			res.json({status:msj.status_false,response:msj.description_incorrect,text:msj.description_incomplete});
	  }
}

// ctrl.edit= async(req,res)=>{
	
// 	const{name,id,image_id}=req.body;
// 	if(name && id){


// 	}
// }






module.exports=ctrl;
